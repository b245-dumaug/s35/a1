const express = require('express');

// Mongoose is a package that allows us to create schemas to model
// our data structures and to manipulate our database using different acess methods.
const mongoose = require('mongoose');

const port = 3001;

const app = express();

// [SECTIOn] MongoDB connection
// Syntax:
/*
mongoose.connect("mongoDBconnectionString", {
  options to avoid errors in our connections
})
 */

mongoose.connect(
  'mongodb+srv://admin:admin@batch245-dumaug.18ijatd.mongodb.net/s35-discussion?retryWrites=true&w=majority',
  {
    // Allows us to avoid any acurrent and future errors while connecting to mongodb.
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

//error handling in connection.
db.on('error', console.error.bind(console, 'Connection error'));

// this will be triggered if the connection is succesful.
db.once('open', () => console.log('We are connected to the cloud database.'));

// Mongoose Schemas
// Schemas determine the structure of the documents to be writtern in the database.
// Shecmas acts as the bluepirnt to our data.
// Syntax
//  const schemaName = new
//  mongoose.Schema({keyvaluepairs})

//default
const taskSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Task name is required'],
  },
  status: {
    type: String,
    default: 'pending',
  },
});

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, 'Task name is required'],
  },
  password: {
    type: String,
    required: true,
  },
});

// [SECTION] Models
// Uses schema to create/instatiate documents/ objects that follows our schema struture.
/*
  The variable/object that will be created can be used to run commands with our database

  Syntax:
    const variableName = mongoose.model('collectionname', schema)
*/

const Task = mongoose.model('Task', taskSchema);

const User = mongoose.model('User', UserSchema);
//middlewares
// it allows the app to read json data.
app.use(express.json());

// Allows our app to read data from forms.
app.use(express.urlencoded({ extended: true }));

// [SECTION] Routing
// Create/add new task
// Check if the task is existing.
// if the task is already exist in the database, we will return a message
// "The task is already existing"
//if the task doenst exist in the database we will add it in the database.

app.post('/tasks', (req, res) => {
  let input = req.body;

  Task.findOne({ name: input.name }, (error, result) => {
    console.log(result);

    if (result !== null) {
      return res.send('The task is already existing');
    } else {
      let newTask = new Task({
        name: input.name,
      });
      // save() method will save the object in the collection that
      //  the object instatiated.
      newTask.save((saveError, savedTask) => {
        if (saveError) {
          return console.log(saveError);
        } else {
          return res.send('New Task created!');
        }
      });
    }
  });
});

// [SECTION] Retrieving the all the tasks
app.get('/tasks', (req, res) => {
  Task.find({}, (error, result) => {
    if (error) {
      console.log(error);
    } else {
      return res.send(result);
    }
  });
});

// activity

app.post('/signup', (req, res) => {
  let input = req.body;

  User.findOne({ username: input.username }, (error, result) => {
    console.log(result);

    if (result !== null) {
      return res.send('User with the same username already exists');
    } else {
      let newUser = new User({
        username: input.username,
        password: input.password,
      });

      newUser.save((saveError, savedUser) => {
        if (saveError) {
          return console.log(saveError);
        } else {
          return res.send('New user created!');
        }
      });
    }
  });
});
app.listen(port, () => console.log(`Server is running at port ${port}!`));
